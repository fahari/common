<?php
/**
 * Created by PhpStorm.
 * User: fardus
 * Date: 26/01/2017
 * Time: 21:09
 */

namespace Fardus\CommonBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;

/**
 * Class AbstractTest
 * @package Fardus\EventsBundle\Tests
 */
abstract class AbstractTest extends WebTestCase
{
    /**
     * @var Container
     */
    protected $container;
    
    /**
    * @var EntityManager
    */
    protected $entityManager;
    
    /**
     * setUp
     */
    protected function setUp(): void
    {
        $client = static::createClient();
        
        $this->container     = $client->getContainer();
        $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
    }
}
