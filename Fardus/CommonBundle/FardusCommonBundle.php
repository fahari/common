<?php

namespace Fardus\CommonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class FardusCommonBundle
 * @package Fardus\CommonBundle
 */
class FardusCommonBundle extends Bundle
{
}
