<?php
/**
 * Created by PhpStorm.
 * User: fardus
 * Date: 08/09/2016
 * Time: 22:47
 */

namespace Fardus\CommonBundle\Tools;

/**
 * Class ConvertToString
 * @package Fardus\CommonsBundle\Tools
 */
class ConvertToString
{
    /**
     * @param $int
     *
     * @return string
     */
    public static function size($int)
    {
        $si_prefix = [ 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' ];
        $base = 1024;
        $class = min((int) log($int, $base), count($si_prefix) - 1);

        return sprintf('%1.2f', $int / pow($base, $class)) . ' ' . $si_prefix[$class];
    }
}
