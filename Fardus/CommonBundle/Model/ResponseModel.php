<?php

namespace Fardus\CommonBundle\Model;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @since   0.5
 * @author  Hamada Sidi Fahari <h.fahari@gmail.com>
 * @package Fardus\CommonBundle\Model
 */
class ResponseModel
{
    /**
     * @var bool
     */
    protected $success;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var array
     */
    protected $data;

    /**
     * Get response default
     *
     * @param array $data
     * @return static
     */
    public static function responseDefault(array $data = []): object
    {
        $response = new static();
        $response->setData($data);
        return $response;
    }

    /**
     * @param ResponseModel $response
     *
     * @return JsonResponse
     */
    public static function jsonResponse(ResponseModel $response): JsonResponse
    {
        $statusCode = Response::HTTP_OK;

        if (!$response->isSuccess()) {
            $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($response->getResult(), $statusCode);
    }

    /**
     * Get Success
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * Set Success
     *
     * @param bool $success
     *
     * @return ResponseRequest
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    /**
     * getResult
     * @return array
     */
    public function getResult(): array
    {
        return [
            'success' => $this->isSuccess(),
            'data' => $this->getData(),
            'message' => $this->getMessage()
        ];
    }

    /**
     * Get Data
     *
     * @return array
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * Set Data
     *
     * @param array $data
     *
     * @return ResponseRequest
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Set Message
     *
     * @param string $message
     *
     * @return ResponseModel
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
