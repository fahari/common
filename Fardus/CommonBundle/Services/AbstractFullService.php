<?php

namespace Fardus\CommonBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AbstractFullService
 *
 * Created by PhpStorm.
 * User: fardus
 * Date: 03/01/2016
 * Time: 08:57
 *
 * @package Fardus\CommonBundle\Services
 */
abstract class AbstractFullService extends AbstractService
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Get EntityManager
     *
     * @return EntityManager
     */
    public function getEntityManager() : ObjectManager
    {
        return $this->entityManager;
    }

    /**
     * Set EntityManager
     *
     * @required
     * @param ObjectManager $entityManager
     * @return static
     */
    public function setEntityManager(ObjectManager $entityManager) : self
    {
        $this->entityManager = $entityManager;

        return $this;
    }
}
