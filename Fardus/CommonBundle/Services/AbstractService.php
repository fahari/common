<?php

namespace Fardus\CommonBundle\Services;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\StopwatchHelper;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractService
 *
 * Created by PhpStorm.
 * User: fardus
 * Date: 03/01/2016
 * Time: 08:57
 *
 * @package Fardus\CommonBundle\Services
 */
abstract class AbstractService
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var StopwatchHelper
     */
    protected $stopWatcher;

    /**
     * Get Logger
     *
     * @return Logger
     */
    public function getLogger() : LoggerInterface
    {
        return $this->logger;
    }

    /**
     * Set Logger
     *
     * @required
     * @param LoggerInterface $logger
     *
     * @return self
     */
    public function setLogger(LoggerInterface $logger) : self
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Get Translator
     *
     * @return TranslatorInterface
     */
    public function trans(string $id, array $parameters = [], string $domain = null, string $locale = null) : string
    {
        return $this->translator->trans($id, $parameters, $domain, $locale);
    }

    /**
     * Get Translator
     *
     * @return TranslatorInterface
     */
    public function getTranslator() : TranslatorInterface
    {
        return $this->translator;
    }

    /**
     * Set Translator
     *
     * @required
     * @param TranslatorInterface $translator
     *
     * @return self
     */
    public function setTranslator(TranslatorInterface $translator) : self
    {
        $this->translator = $translator;

        return $this;
    }

    /**
     * Get StopWatcher
     *
     * @return StopwatchHelper
     */
    public function getStopWatcher() : StopwatchHelper
    {
        return $this->stopWatcher;
    }

    /**
     * Set StopWatcher
     *
     * @param StopwatchHelper $stopWatcher
     *
     * @return AbstractService
     */
    public function setStopWatcher(StopwatchHelper $stopWatcher) : self
    {
        $this->stopWatcher = $stopWatcher;

        return $this;
    }
}
