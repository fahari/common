<?php

namespace Fardus\CommonBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class NameEntity
 * @package Fardus\CommonBundle\Traits
 */
trait NameEntity
{
    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * Gets the value of name.
     *
     * @return string
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param string $name the name
     *
     * @return static
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }
}
