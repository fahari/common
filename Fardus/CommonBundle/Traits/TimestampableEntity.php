<?php

namespace Fardus\CommonBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class TimestampableEntity
 *
 * Created by PhpStorm.
 * User: fardus
 * Date: 28/01/2016
 * Time: 18:26
 *
 * @package App\Entity\Traits
 */
trait TimestampableEntity
{

    /**
     * @var \DateTimeInterface $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $createdAt;

    /**
     * @var \DateTimeInterface $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $updatedAt;

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreatedAt() : ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdatedAt() : ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * Set updated
     *
     * @param \DateTimeInterface $updated
     * @return self
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
