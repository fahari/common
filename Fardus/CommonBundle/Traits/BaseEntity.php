<?php

namespace Fardus\CommonBundle\Traits;

/**
 * Class BaseEntity
 * @package Fardus\CommonBundle\Traits
 */
trait BaseEntity
{
    use NameEntity, EnableEntity, TimestampableEntity;
}
