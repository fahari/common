<?php

namespace Fardus\CommonBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * Class BaseEntity
 * @package Fardus\CommonBundle\Traits
 */
trait PersonEntity
{
    use BaseEntity;
    use SoftDeleteableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="PRS_ID", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $forname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $birthday;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $birthplace;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column( type="string", length=10, nullable=true)
     */
    protected $zip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * Get author
     *
     * @return int
     */
    public function getAge() : ?int
    {
        return $this->birthday->diff(new \DateTime())->y;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getNameComplete() : string
    {
        return strtoupper($this->name).' '.ucwords($this->forname);
    }

    /**
     * Set forname
     *
     * @param string $forname
     * @return Person
     */
    public function setForname(string $forname) : self
    {
        $this->forname = $forname;

        return $this;
    }

    /**
     * Get forname
     *
     * @return string
     */
    public function getForname() : ?string
    {
        return $this->forname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Person
     */
    public function setEmail(string $email = null) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() : ?string
    {
        return $this->email;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Person
     */
    public function setBirthday(\DateTime $birthday = null) : self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday() : ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * Set birthplace
     *
     * @param string $birthplace
     * @return Person
     */
    public function setBirthplace(string $birthplace = null) : self
    {
        $this->birthplace = $birthplace;

        return $this;
    }

    /**
     * Get birthplace
     *
     * @return string
     */
    public function getBirthplace() : ?string
    {
        return $this->birthplace;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Person
     */
    public function setGender(string $gender = null) : self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function hasGender() : bool
    {
        return !empty($this->getGender());
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender() : ?string
    {
        $gender = $this->gender;

        if(in_array($gender, ['feminin', 'masculin'])) {
            $gender =  $gender === 'masculin' ? Person::GENDER_MALE : self::GENDER_FEMALE;
        }

        return $gender ;
    }


    /**
     * Set address
     *
     * @param string $address
     * @return Person
     */
    public function setAddress(string $address = null) : self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() : ?string
    {
        return $this->address;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Person
     */
    public function setZip(string $zip = null) : self
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip() : ?string
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Person
     */
    public function setCity(string $city = null) : self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() : ?string
    {
        return $this->city;
    }

}
