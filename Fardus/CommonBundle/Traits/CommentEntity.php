<?php

namespace Fardus\CommonBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CommentEntity
 * @package Fardus\CommonBundle\\Traits
 */
trait CommentEntity
{
    /**
     * @var string $comment
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * Gets the value of comment.
     *
     * @return string
     */
    public function getComment() : ?string
    {
        return $this->comment;
    }

    /**
     * Sets the value of comment.
     *
     * @param string $comment the comment
     *
     * @return static
     */
    public function setComment(string $comment = null) : self
    {
        $this->comment = $comment;

        return $this;
    }
}
