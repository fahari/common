<?php

namespace Fardus\CommonBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class DescriptionEntity
 * @package Fardus\CommonBundle\\Traits
 */
trait DescriptionEntity
{
    /**
     * @var string $description
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * Gets the value of description.
     *
     * @return string
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param string $description the description
     *
     * @return static
     */
    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }
}
