<?php

namespace Fardus\CommonBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EnableEntity
 * @package Fardus\AdminBundle\Entity\Traits
 */
trait EnableEntity
{
    /**
     * @var bool $enable
     *
     * @ORM\Column(type="boolean")
     */
    protected $enable = true;

    /**
     * Gets the value of enable.
     *
     * @return bool
     */
    public function getEnable() : ?bool
    {
        return $this->enable;
    }

    /**
     * Gets the value of enable.
     *
     * @return bool
     */
    public function isEnable() : ?bool
    {
        return $this->enable === true;
    }

    /**
     * Sets the value of enable.
     *
     * @param bool $enable the enable
     *
     * @return static
     */
    public function setEnable(bool $enable) : self
    {
        $this->enable = $enable;

        return $this;
    }
}
