<?php
/**
 * Created by PhpStorm.
 * User: fardus
 * Date: 21/03/2016
 * Time: 20:05
 */

namespace Fardus\CommonBundle\Exception;

/**
 * Class InvalidArgumentException
 * @package Fardus\CommonsBundle\Exception
 */
class InvalidArgumentException extends FardusException
{
}
