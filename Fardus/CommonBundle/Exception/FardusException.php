<?php

namespace Fardus\CommonBundle\Exception;

/**
 * Created by PhpStorm.
 * User: fardus
 * Date: 21/03/2016
 * Time: 20:05
 *
 * Class FardusException
 * @package Fardus\CommonsBundle\Exception
 */
class FardusException extends \Exception
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getMessage();
    }
}
